
uses ConlusionResultsOnScreen, OperationsOnData;

Var
  RepositoryFile:mas;
  NumbersOfFile:integer;

begin
  randomize;
  GettingNumbersOfFiles (NumbersOfFile);
  FormationOfFileSize(RepositoryFile,NumbersOfFile);
  PrintRepositoryFileOnScreen(RepositoryFile,NumbersOfFile);
  PrintMaximumOnScreen(RepositoryFile,NumbersOfFile);
  PrintMinimumOnScreen(RepositoryFile,NumbersOfFile);
  PrintArifmeticalMeanOnScreen(RepositoryFile,NumbersOfFile);
  PrintSumOnScreen(RepositoryFile, NumbersOfFile);
  PrintResultOfCheckOnScreen(RepositoryFile);
  readln;
end.
