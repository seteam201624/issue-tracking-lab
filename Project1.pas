
uses ConlusionResultsOnScreen, OperationsOnData;

Var
  RepositoryFile:mas;

begin
  randomize;
  FormationOfFileSize(RepositoryFile);
  PrintRepositoryFileOnScreen(RepositoryFile);
  PrintMaximumOnScreen(RepositoryFile);
  PrintMinimumOnScreen(RepositoryFile);
  PrintArifmeticalMeanOnScreen(RepositoryFile);
  PrintResultOfCheckOnScreen(RepositoryFile);
  readln;
end.
