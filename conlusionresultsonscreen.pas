
unit ConlusionResultsOnScreen;

interface

uses OperationsOnData;

const
  ConstantNumber=1024;

type
  mas=array[1..NumbersOfFile] of integer;

procedure PrintRepositoryFileOnScreen(RepositoryFile:mas; NumbersOfFile:integer);
procedure PrintMinimumOnScreen(RepositoryFile:mas;NumbersOfFile:integer);
procedure PrintMaximumOnScreen(RepositoryFile:mas;NumbersOfFile:integer);
procedure PrintArifmeticalMeanOnScreen(RepositoryFile:mas;NumbersOfFile:integer);
procedure PrintSumOnScreen(RepositoryFile:mas; NumbersOfFile:integer);
procedure PrintResultOfCheckOnScreen(RepositoryFile:mas);

implementation

procedure PrintRepositoryFileOnScreen(RepositoryFile:mas; NumbersOfFile:integer);
Var
  i:integer;
begin
  for i:=1 to NumbersOfFile do
  begin
    if (RepositoryFile[i]<ConstantNumber) then
      writeln ('Size ',i,' file = ', RepositoryFile[i],'K')
    else
      if (RepositoryFile[i] mod ConstantNumber = 0) then
        writeln ('Size ',i,' file = ', RepositoryFile[i]/ConstantNumber,'M')
      else
        writeln ('Size ',i,' file = ', RepositoryFile[i] div ConstantNumber,'M  ',RepositoryFile[i] mod ConstantNumber,'K');
  end;
end;

procedure PrintMinimumOnScreen(RepositoryFile:mas; NumbersOfFile:integer);
begin
  writeln('Minimum Size Of File = ',FindMinimum(RepositoryFile,NumbersOfFile) div ConstantNumber,'M  ',FindMinimum(RepositoryFile,NumbersOfFile) mod ConstantNumber,'K');
end;

procedure PrintMaximumOnScreen(RepositoryFile:mas; NumbersOfFile:integer);
begin
  writeln('Maximum Size Of File = ',FindMaximum(RepositoryFile,NumbersOfFile) div ConstantNumber,'M  ', FindMaximum(RepositoryFile,NumbersOfFile) mod ConstantNumber,'K');
end;

procedure PrintArifmeticalMeanOnScreen(RepositoryFile:mas; NumbersOfFile:integer);
begin
  writeln('Average Size Of File = ',FindArifmeticalMean(RepositoryFile,NumbersOfFile) div ConstantNumber,'M  ',FindArifmeticalMean(RepositoryFile,NumbersOfFile) mod ConstantNumber,'K');
end;

procedure PrintSumOnScreen(RepositoryFile:mas; NumbersOfFile:integer);
begin
  writeln ('Sum All Sizes Of Files = ',FindingTotalSizeOfFiles(RepositoryFile, NumbersOfFile) div ConstantNumber,'M  ',FindingTotalSizeOfFiles(RepositoryFile, NumbersOfFile) mod ConstantNumber,'K');
end;

procedure PrintResultOfCheckOnScreen(RepositoryFile:mas);
begin
  if (CheckComplianceOfSize(RepositoryFile)) then
    writeln('Yes')
  else
    writeln ('No');
end;

end.

