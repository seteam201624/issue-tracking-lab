unit OperationsOnData;

interface

const
  NumbersOfFile=1000;
  ConstantNumber=1024;
  EndRange=300;

type
  mas=array[1..NumbersOfFile] of integer;

procedure GettingNumbersOfFiles (Var NumbersOfFile:integer);
procedure FormationOfFileSize(Var RepositoryFile:mas; NumbersOfFile:integer);
function FindMinimum(RepositoryFile:mas; NumbersOfFile:integer):integer;
function FindMaximum(RepositoryFile:mas; NumbersOfFile:integer):integer;
function FindArifmeticalMean(RepositoryFile:mas; NumbersOfFile:integer):integer;
function FindingTotalSizeOfFiles(RepositoryFile:mas; NumbersOfFile:integer):integer;
function GettingSizeOfFolder:integer;
function CheckComplianceOfSize(RepositoryFile:mas):boolean;

implementation

procedure GettingNumbersOfFiles (Var NumbersOfFile:integer);
begin
  write('Enter Numbers Of Files: ');
  readln(NumbersOfFile);
end;

procedure FormationOfFileSize(Var RepositoryFile:mas; NumbersOfFile:integer);
Var
  i:integer;
begin
  for i:=1 to NumbersOfFile do
  begin
    RepositoryFile[i]:=random(EndRange*ConstantNumber);
  end;
end;

function FindMinimum(RepositoryFile:mas; NumbersOfFile:integer):integer;
Var
  Minimum:longint;
  i:integer;
begin
  Minimum:=RepositoryFile[1];
  for i:=2 to NumbersOfFile do
    if (Minimum>RepositoryFile[i]) then
      Minimum:=RepositoryFile[i];
  FindMinimum:=Minimum;
end;

function FindMaximum(RepositoryFile:mas; NumbersOfFile:integer):integer;
Var
  Maximum:longint;
  i:integer;
begin
  Maximum:=RepositoryFile[1];
  for i:=2 to NumbersOfFile do
  begin
    if (Maximum<RepositoryFile[i]) then
    Maximum:=RepositoryFile[i];
  end;
  FindMaximum:=Maximum;
end;

function FindingTotalSizeOfFiles(RepositoryFile:mas; NumbersOfFile:integer):integer;
Var
  Sum:longint;
  i:integer;
begin
  Sum:=0;
  for i:=1 to NumbersOfFile do
    Sum:=Sum+RepositoryFile[i];
  FindingTotalSizeOfFiles:=Sum;
end;

function FindArifmeticalMean(RepositoryFile:mas; NumbersOfFile:integer):integer;
begin
  FindArifmeticalMean:=FindingTotalSizeOfFiles(RepositoryFile,NumbersOfFile) div NumbersOfFile;
end;

function GettingSizeOfFolder:integer;
Var
  Ch,FolderSize:string;
  Code:integer;
  Size:longint;
  b:boolean;
begin
  b:=true;
  while (b) do
  begin
    write ('Enter the folder size - ');
    readln(FolderSize);
    Ch:=copy(FolderSize,length(FolderSize),1);
    delete(FolderSize,length(FolderSize),1);
    Val(FolderSize,Size,Code);
    case Ch of
      'G': begin Size:=Size*ConstantNumber*ConstantNumber; b:=false; end;
      'M': begin Size:=Size*ConstantNumber; b:=false; end;
    else
      begin
        writeln('Incorrect data entry. Try again');
        b:=true;
      end;
    end;
  end;
  GettingSizeOfFolder:=Size;
end;

function CheckComplianceOfSize(RepositoryFile:mas):boolean;
begin
  if (GettingSizeOfFolder<FindingTotalSizeOfFiles(RepositoryFile,NumbersOfFile)) then
    CheckComplianceOfSize:=true
  else
    CheckComplianceOfSize:=false;
end;

end.
